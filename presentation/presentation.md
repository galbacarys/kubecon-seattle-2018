---
theme: "solarized"
transition: "fade"
highlightTheme: "darkula"
---
# Serverless Infrastructure

Or, Lambdas replacing Humans

<small>

Gabe Albacarys (@itsgabethebando)

KubeCon Seattle 2018

</small>

notes:

* Introduce Self
* Introduce who I am, what I do

--

## Who even are you?

Gabe Albacarys

Twitter: @itsgabethebando

Everywhere else: @galbacarys

Software engineer at PENDING

I do Kubernetes stuff

notes:

* Talk about whatever projects I'm working on (will likely be different than today)

---

# What do we do when Kubernetes misbehaves?

notes:

* SPONGEBOB GIF IS COMING UP NEXT DO NOT FREAK OUT
* most of us likely do some variation on this (advance slide)

--

<!-- .slide: data-background-video="https://media.giphy.com/media/nrXif9YExO9EI/giphy.mp4" data-background-color="#000000" data-background-video-loop="loop" -->

notes:

We panic. If you're being honest, what would you do if your control plane became unresponsive tomorrow?

Do you have a backup plan? Do you have a playbook for recovering your cluster quickly and correctly?

Are people trained? Are you taking backups?

Do you even know if any of this would work? Have you ever done a fire drill in your production systems?

---

# What SHOULD we do when Kubernetes misbehaves?

--

<!-- .slide: data-background-video="https://media.giphy.com/media/720g7C1jz13wI/giphy.mp4" data-background-color="#000000" data-background-video-loop="loop" -->

--

Let it be annoying. Don't let it ruin your day.

---

# The problem

--

We're software engineers and ops heads.

We like to be lazy.

--

<img src="https://amorphia-apparel.com/image/rex.1200.png" style="height:500px;">

"Kubernetes is awesome! Why not use it for everything?"

--

<img src="https://i.imgur.com/0Y43DsV.jpg" style="height:500px;">

This causes *problems*.

---

# A nightmare Scenario

--

<small>3:30 AM</small>

Your teammate in Dublin tries to push a new change to production.

--

<small>3:35 AM</small>

Their deployment fails.

--

<small>3:40 AM</small>

They decide to poke at the prod cluster to see what's happening.